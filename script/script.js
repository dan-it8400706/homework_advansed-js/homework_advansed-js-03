// Завдання 1.1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const clientsFullBase = new Set(clients1.concat(clients2))
console.log(clientsFullBase)



// Завдання 2
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];


let charactersShortInfo = characters.map(({name,lastName,age},index, array)=>({name,lastName,age}))

    console.log(charactersShortInfo)


// Завдіння 3

const user1 = {
    name: "John",
    years: 30
    };
let {name:ім_я,years:вік,isAdmin:isAdmin=false}=user1
let user2  = {ім_я, вік, isAdmin}
//  Ім'я має містити лише букви, цифри або символи $ і _. 
//  Так,  я читав про екранування з \, і метод "'", а також знайшов u+2019.. але екранувати так і не зміг
//  Як це робити... ? 

console.log(user2)

// Завдання 4

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020}
console.log(fullProfile)


// Завдання 5

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

const booksAll = [...books,bookToAdd]

console.log(booksAll)


// Завдання 6

let employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }




const personeCard = {...employee, age : 18 ,salary : 100 }
console.log(personeCard)


// Завдання 7


const array = ['value', () => 'showValue'];

// Допишіть код тут
let [value, showValue] = array


alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'